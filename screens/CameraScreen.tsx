import * as React from 'react';
import { StyleSheet, TouchableOpacity, Alert } from 'react-native'; 
import { Text, View } from '../components/Themed';
import {Camera} from 'expo-camera'
let camera: Camera

export default function CameraScreen() {

  const [startCamera, setStartCamera] = React.useState(false)
  
  const __startCamera = async ()=>{
    const {status} = await Camera.requestCameraPermissionsAsync()
    if(status === 'granted'){
      // do something
      setStartCamera(true)
    }else{
      Alert.alert("Access denied")
    }
  }
  return (
    <View style={styles.container}>
      {startCamera ? (
        <Camera
          style={{flex: 1,width:"100%"}}
          ref={(r) => {
            camera = r
          }}
        >
      </Camera>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <TouchableOpacity
            onPress={__startCamera}
            style={{
              width: 130,
              borderRadius: 4,
              backgroundColor: '#14274e',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              height: 40
            }}
          >
            <Text
              style={{
                color: '#fff',
                fontWeight: 'bold',
                textAlign: 'center'
              }}
            >
              Mostrar Camara
            </Text>
          </TouchableOpacity>
        </View>
      )}
      </View>
  );
  }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
