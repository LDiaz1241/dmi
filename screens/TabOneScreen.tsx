import * as React from 'react';
import { StyleSheet, FlatList } from 'react-native';

import { Text, View} from '../components/Themed';
import { RootTabScreenProps } from '../types';

export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Proyecto Desarrollo Movil Integral</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <Text style={styles.title}>Miembros del Equipo</Text>
      <FlatList
        data={[
          {key: 'Luis Alberto Díaz de León Ochoa'},
          {key: 'Brandon Enrique Rejón Calleja'},
          {key: 'Ernesto de Jesus Moreno Chin'},
          {key: 'Michel Hernandez Puc'},
        ]}
        renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
